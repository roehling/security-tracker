A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
asterisk/oldstable
--
chromium
--
condor
--
faad2/oldstable (jmm)
--
linux (carnil)
  Wait until more issues have piled up, though try to regulary rebase for point
  releases to more recent v4.19.y versions.
--
ndpi/oldstable
--
nodejs (jmm)
--
python-pysaml2 (jmm)
--
redis (jmm)
--
rpki-client/stable
  new 7.6 release required libretls, which isn't in Bullseye
--
runc
--
trafficserver (jmm)
  wait until status for CVE-2021-38161 is clarified (upstream patch got reverted)
--
varnish (fw)
--
webkit2gtk (berto)
--
wpewebkit (berto)
--
zsh (seb)
--
